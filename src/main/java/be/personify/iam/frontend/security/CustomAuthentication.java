package be.personify.iam.frontend.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomAuthentication implements Authentication{

	private static final long serialVersionUID = -2465661964089021764L;
	
	private String principal;
	private String credentials;
	private boolean authenticated;
	private UserDetails details;
	private long validityInMillisecons;
	private long creationTimeInMilliseconds;
	private boolean multiFactorRequired;
	private boolean multiFactorFinished;
	
	/**
	 * Constructor
	 * @param principal the username ( email )
	 * @param credentials the token
	 * @param validityInMilliseconds the validity
	 */
	public CustomAuthentication( String principal, String credentials, long validityInMilliseconds) {
		this.principal = principal;
		this.credentials = credentials;
		this.creationTimeInMilliseconds = System.currentTimeMillis();
		this.validityInMillisecons = validityInMilliseconds;
	}
	
		
	@Override
	public String getName() {
		return principal;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getCredentials() {
		return credentials;
	}
	
	public void setDetails( UserDetails customDetails ) {
		this.details = customDetails;
	}
	 

	@Override
	public Object getDetails() {
		return details;
	}

	@Override
	public Object getPrincipal() {
		return principal;
	}

	@Override
	public boolean isAuthenticated() {
		if( authenticated ) {
			if ( System.currentTimeMillis() < (creationTimeInMilliseconds + validityInMillisecons)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}


	public boolean isMultiFactorRequired() {
		return multiFactorRequired;
	}


	public void setMultiFactorRequired(boolean multiFactorRequired) {
		this.multiFactorRequired = multiFactorRequired;
	}


	public boolean isMultiFactorFinished() {
		return multiFactorFinished;
	}


	public void setMultiFactorFinished(boolean multiFactorFinished) {
		this.multiFactorFinished = multiFactorFinished;
	}
	
	

}
