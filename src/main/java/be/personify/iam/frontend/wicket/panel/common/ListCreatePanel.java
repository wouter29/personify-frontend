package be.personify.iam.frontend.wicket.panel.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import be.personify.util.StringUtils;

/**
 * Panel for a List
 * @author vanderw
 *
 * @param <T> the type
 */
public class ListCreatePanel<T> extends Panel {
	

	private static final long serialVersionUID = -5234628747305500756L;

	private static final String READONLY = "readonly";

	private static final String LIST_PANEL_FORM = "listPanelForm";

	private static final String DATA_VIEW_CONTAINER = "dataViewContainer";

	private static final String NEW_VALUE = "new_value";

	private static final String DATA_VIEW = "dataView";

	
	private List<String> valueList = new ArrayList<String>();
	
	private MarkupContainer dataViewContainer;
	private DataView<String> dataView;
	
	/**
	 * Constructor
	 * @param id the identifier
	 * @param model the model
	 */
	public ListCreatePanel( String id, final IModel<T> model ) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		Form form = new Form(LIST_PANEL_FORM);
		
		dataViewContainer = new WebMarkupContainer(DATA_VIEW_CONTAINER);
		dataViewContainer.setOutputMarkupId(true);
		
		List<String> list = (List<String>)getDefaultModel().getObject();
		if ( list != null ) {
			for (String value : list) {
				valueList.add(value);
			}
		}
		
		dataView = getDataView(DATA_VIEW, createDataProvider() );
		dataViewContainer.add(dataView);
		
		TextField<String> valueTextField = new TextField<String>(NEW_VALUE, new Model<String>(StringUtils.EMPTY_STRING));		
		dataViewContainer.add(valueTextField);
		
		dataViewContainer.add(new AjaxButton(PanelConstants.BUTTON_ADD) {
			private static final long serialVersionUID = -5272734134708203645L;

			
			
			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				
				String value = valueTextField.getModelObject();
				if ( !StringUtils.isEmpty(value)) {
					if ( !valueList.contains(value)) {
						valueList.add(valueTextField.getModelObject());
						((List<String>)ListCreatePanel.this.getDefaultModel().getObject()).add(valueTextField.getModelObject());			
						valueTextField.setModelObject(StringUtils.EMPTY_STRING);
						dataView = getDataView(DATA_VIEW,createDataProvider() );
						dataViewContainer.addOrReplace(dataView);
					}
				}
				target.add(dataViewContainer);
				super.onSubmit(target);
			}
			
		});
		form.add(dataViewContainer);
		add(form);
	}

	
	

	/**
	 * Creates a dataprovider
	 * @return a list dataprovider
	 */
	private ListDataProvider<String> createDataProvider() {
				
		ListDataProvider<String> dp = new ListDataProvider<String>(valueList) {

			private static final long serialVersionUID = -4570054503756352135L;

			@Override
			protected List<String> getData() {
				return super.getData();
			}

			@Override
			public IModel<String> model(String object) {
				if ( object != null ) {
					for ( String value : valueList) {
						if ( value.equals(object)) {
							value = object;
							return super.model(value);
						}
					}
				}
				return super.model(object);
				
			}
			
		};
		return dp;
	} 
	
	
	
	
	
	/**
	 * Creates a dataview
	 * @param name the name of the dataview
	 * @param dp a list with the dataproviders
	 * @return a dataview
	 */
	public DataView<String> getDataView(String name, ListDataProvider<String> dp) {

	
		DataView<String> view = new DataView<String>(name, dp) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<String> item) {
				
				//the textfield
				TextField<String> tf = new TextField<String>(PanelConstants.VALUE, new Model<String>(item.getModelObject()) {

					private static final long serialVersionUID = -1765624247667019938L;

					@Override
					public void setObject(String object) {
						String value = item.getModelObject();
						List<String> currentList = ((List<String>)ListCreatePanel.this.getDefaultModel().getObject());
						if ( !currentList.contains(value)) {
							currentList.add(value);
						}
						super.setObject(object);
					}
					
				});
				if ( !StringUtils.isEmpty(item.getModelObject())) {
					tf.add(AttributeModifier.replace(READONLY, READONLY));
				};
				
				item.add(tf);
				item.add(new AjaxButton(PanelConstants.BUTTON_DELETE) {
					
					private static final long serialVersionUID = -5272734134708203645L;

					@Override
					protected void onSubmit(AjaxRequestTarget target) {
						
						valueList.remove(item.getModelObject());
						((List<String>)ListCreatePanel.this.getDefaultModel().getObject()).remove(item.getModelObject());
						dataView = getDataView(DATA_VIEW, createDataProvider() );
						dataViewContainer.addOrReplace(dataView);
						target.add(dataViewContainer);
						super.onSubmit(target);
					}
					
				});
			}
		};
		view.setOutputMarkupId(true);
		view.setOutputMarkupPlaceholderTag(true);
		view.setItemsPerPage(200);
		return view;
	}
	
	
	

}
