package be.personify.iam.frontend.wicket.panel.common;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalDialog;

public class GenericModalWindow extends ModalDialog {
	
	private static final long serialVersionUID = 3150224139209725731L;
	

	public GenericModalWindow(String id ) {
		super(id);
		
		//setResizable(true);
		//setInitialWidth(40);
		//setInitialHeight(50);
		//setWidthUnit("em");
		//setHeightUnit("em");
		
		//setCookieName("generic_modal_" + id);
		
		//setCssClassName(ModalWindow.CSS_CLASS_GRAY);
	}
	
	

}
