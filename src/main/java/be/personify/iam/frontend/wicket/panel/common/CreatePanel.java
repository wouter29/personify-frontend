package be.personify.iam.frontend.wicket.panel.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import be.personify.iam.api.util.Criterium;
import be.personify.iam.api.util.CriteriumOperator;
import be.personify.iam.api.util.Query;
import be.personify.iam.api.util.QueryOperator;
import be.personify.util.StringUtils;

public abstract class CreatePanel<T> extends FormPanel<T> {

	private static final String DISPLAY_FIELDS = "display_fields";

	private static final String SEARCH_FIELDS = "search_fields";

	private static final String FORM_FEEDBACK_PANEL = "formFeedbackPanel";

	private static final long serialVersionUID = 1725217325511287056L;
	
	private static final Logger logger = LogManager.getLogger(CreatePanel.class);
	
	private static final ExpressionParser expressionParser = new SpelExpressionParser();

	private Form<T> form;
	
	public FeedbackPanel feedbackPanel;
	
	public CreatePanel(String id) {
		super(id);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		//add the feedbackpanel for the form
		feedbackPanel = new FeedbackPanel(FORM_FEEDBACK_PANEL); 
		feedbackPanel.setOutputMarkupId(true);
		feedbackPanel.setOutputMarkupPlaceholderTag(true);
		add( feedbackPanel );
		
		form = initForm();
		add(form);
	}
	
	public abstract Form<T> initForm();
	
	public abstract void create();
	
	
	protected Query getSearchQueryForAutoComplete( String input, String autocompleConfig ) {
		
		String[] zz = autocompleConfig.split(StringUtils.ESCAPED_PIPE);
		String[] searchFields = null;
		for ( String z : zz ) {
			if ( z.startsWith(SEARCH_FIELDS)) {
				searchFields = z.split(StringUtils.EQUALS)[1].split(StringUtils.COMMA);
			}
		}
		
		Query query = new Query();
		query.setOperator(QueryOperator.OR);
		
		for ( String s : searchFields) {
			logger.debug("adding criterium {} ", s);
			query.addCriterium(new Criterium(s, StringUtils.PERCENT + input + StringUtils.PERCENT, CriteriumOperator.LIKE));
		}
		
		return query;
		
	}
	
	
	
	
	public String getDisplayFieldsForAutoComplete( String autoCompleteConfig, Object object  ) {
		
		logger.debug("autoCompleteConfig {} ", autoCompleteConfig);
		
		
		String[] zz = autoCompleteConfig.split(StringUtils.ESCAPED_PIPE);
		String[] displayFields = null;
		for ( String z : zz ) {
			logger.debug("z {}", z);
			if ( z.startsWith(DISPLAY_FIELDS)) {
				displayFields = z.split(StringUtils.EQUALS)[1].split(StringUtils.COMMA);
			}
		}
		
		logger.info("displayFields length {}", displayFields.length);
		
		StringBuffer result = new StringBuffer();
		
		EvaluationContext context = new StandardEvaluationContext(object);
		for ( int i= 0; i < displayFields.length; i++) {
			String s = displayFields[i];
			Expression expression = expressionParser.parseExpression(s);
			if ( i == displayFields.length -2 ) {
				result.append((String) expression.getValue(context)).append(StringUtils.SPACE_DASH_SPACE);
			}
			else {
				if ( i < displayFields.length -1 ){
					result.append((String) expression.getValue(context)).append(StringUtils.SPACE);
				}
				else {
					result.append((String) expression.getValue(context));
				}
			}
		}
		return result.toString();
	}
	
	
	
	
}
