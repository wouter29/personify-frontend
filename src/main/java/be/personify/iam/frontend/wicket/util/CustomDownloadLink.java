package be.personify.iam.frontend.wicket.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.wicket.markup.html.link.DownloadLink;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;

import be.personify.util.StringUtils;

public class CustomDownloadLink extends DownloadLink{

	private static final long serialVersionUID = -5779766712283542869L;
	private byte[] bytes = null;
	private String fileName = null;
	
	public CustomDownloadLink(String id, byte[] bytes, String fileName ) {
		super(id, new File(StringUtils.EMPTY_STRING));
		this.bytes = bytes;
		this.fileName = fileName;
	}
	
	
	@Override
    public void onClick() {
        File downloadFile = new File(fileName); 
        try {
			FileUtils.writeByteArrayToFile(downloadFile, bytes);
		}
        catch (IOException e) {
			e.printStackTrace();
		}
        IResourceStream resourceStream = new FileResourceStream(new org.apache.wicket.util.file.File(downloadFile));
        getRequestCycle().scheduleRequestHandlerAfterCurrent
                (new ResourceStreamRequestHandler(resourceStream).
                        setFileName(downloadFile.getName()));
    }
	

}
