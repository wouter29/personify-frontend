package be.personify.iam.frontend.wicket.util;

import org.apache.wicket.markup.html.basic.Label;

import be.personify.util.io.IOUtils;

/**
 * Basic class for displaying a date
 * @author vanderw
 *
 */
public class FileSizeLabel extends Label {

	private static final long serialVersionUID = 7191335790488417183L;
	
	

	public FileSizeLabel(String id, long fileSize) {
		super(id, IOUtils.humanReadableByteCountBin(fileSize) );
	}
	
	
	


}
