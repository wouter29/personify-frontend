package be.personify.iam.frontend.wicket.panel.common;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

import be.personify.iam.api.util.LinkUtil;
import be.personify.iam.frontend.wicket.model.common.WicketEntityModel;
import be.personify.util.Action;
import be.personify.util.Decision;
import be.personify.util.PolicyEvaluator;
import be.personify.util.StringUtils;

/**
 * A abstract resource panel
 * @author vanderw
 *
 * @param <T> the generic type
 */
public abstract class EntityPanel<T> extends Panel {
	
	private static final Logger logger = LogManager.getLogger(EntityPanel.class);

	private static final long serialVersionUID = -7268964531051616124L;
	
	
	public static final String SALT_KEY = "h";
	
	protected static final String PANEL_BODY_NAME = "panel-body";
	
	protected static final String EXPAND_LINK = "expandLink";
	

	@SpringBean
	private Environment environment;

	private boolean showRelations;
	
	private boolean expanded;
	
	private String parentName;

	public static final ClassInstanciator ci = new ClassInstanciator();

	public EntityPanel(String id, WicketEntityModel<EntityModel<T>> model) {
		super(id, model);
		Injector.get().inject(this);
	}

	public EntityPanel(String id) {
		super(id);
		Injector.get().inject(this);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
	}

	public void setCustomModel(IModel model) {
		setDefaultModel(model);
	}

	public void setCustomModel(WicketEntityModel<EntityModel<T>> model) {
		setDefaultModel(model);
	}

	public WicketEntityModel<EntityModel<T>> getModel() {
		return (WicketEntityModel<EntityModel<T>>) getDefaultModel();
	}

	public boolean isShowRelations() {
		return showRelations;
	}

	public void setShowRelations(boolean showRelations) {
		this.showRelations = showRelations;
	}
	
	
	
	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	

	/**
	 * 
	 * @param parentName the name of the parent
	 * @param model the model
	 * @param withRelations withrelations 
	 * @param policyEvaluator the policyevaluator
	 * @param viewFilter the viewfilter
	 */
	public void addRelations(String parentName, WicketEntityModel<EntityModel<T>> model, boolean withRelations, PolicyEvaluator policyEvaluator, List<String> viewFilter ) {
		logger.debug("addRelations withrelations {}",  withRelations);
		
		List<Link> filteredLinks = LinkUtil.filterSelf(model.getLinks());
		if ( filteredLinks != null ) {
			EntitiesPanel<EntityModel<?>> resourcesPanel = null;
			for (Link link : filteredLinks) {
				logger.debug("addRelations link rel value {} ", link.getRel().value());
				if ( viewFilter.contains(link.getRel().toString())) {
					logger.debug("viewfilter contains");
					add(new EmptyPanel(link.getRel().value()).setVisible(false));
				}
				else {
					logger.debug("viewfilter NOT contains");
					Panel panel = ci.getPanel(parentName, link, withRelations);
					
					logger.debug("addRelations panel {} ", panel);
					boolean allowed = policyEvaluator.evaluate(Decision.ALLOW, Action.READ, link.getRel().value());
		
					if ( panel == null || !allowed ) {
						add(new EmptyPanel(link.getRel().value()));
					}
					else if (panel instanceof EntitiesPanel) {
						logger.debug("it's a entitypanel");
						resourcesPanel = (EntitiesPanel) ci.getPanel(parentName, link, false);
						if (resourcesPanel != null) {
							if (resourcesPanel.getCustomModel().getCount() > 0 && withRelations) {
								add(resourcesPanel);
							} 
							else {
								logger.debug("panel is null for link " + link.getHref() + StringUtils.SPACE + link.getRel());
								add(new EmptyPanel(link.getRel().value()));
							}
						}
					}
				}
			}
		}
	}
	
	

	public Link findLink(String type, List<Link> links) {
		for (Link link : links) {
			if (link.getRel().equals(type)) {
				return link;
			}
		}
		return null;
	}

}
