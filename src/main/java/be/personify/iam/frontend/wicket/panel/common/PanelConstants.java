package be.personify.iam.frontend.wicket.panel.common;

public class PanelConstants {
	
	public static final String CREATE_FORM = "CreateForm";
	public static final String ROW = "Row";
	public static final String CONTENT_DOT = "content.";
	public static final String SUBMIT = "submit";   
	
	public static final String BUTTON_ADD = "add";
	public static final String BUTTON_DELETE = "delete";
	public static final String BUTTON_EDIT = "edit";
	public static final String BUTTON_SUBMIT = "submit";
	
	public static final String ADD_FORM = "addForm";
	
	public static final String SELF = "self";
	
	public static final String VALUE = "value";
	public static final String KEY = "key"; 
	
	public static final String HEADER = "header";
	
	public static final String ID = "id";
 
}
