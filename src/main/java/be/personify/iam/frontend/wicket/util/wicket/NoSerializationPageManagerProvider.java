package be.personify.iam.frontend.wicket.util.wicket;

import org.apache.wicket.Application;
import org.apache.wicket.DefaultPageManagerProvider;
import org.apache.wicket.page.IManageablePage;
import org.apache.wicket.pageStore.IPageContext;
import org.apache.wicket.pageStore.IPageStore;

public class NoSerializationPageManagerProvider extends DefaultPageManagerProvider {

    @Override
	protected IPageStore newSerializingStore(IPageStore pageStore) {
		// TODO Auto-generated method stub
    	return new IPageStore() {
            @Override 
            public void destroy() {
            }

			@Override
			public boolean supportsVersioning() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void addPage(IPageContext context, IManageablePage page) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removePage(IPageContext context, IManageablePage page) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeAllPages(IPageContext context) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public IManageablePage getPage(IPageContext context, int id) {
				// TODO Auto-generated method stub
				return null;
			} 

            
        }; 
	}



	public NoSerializationPageManagerProvider(Application application) {
        super(application); 
    } 
    
    

}
