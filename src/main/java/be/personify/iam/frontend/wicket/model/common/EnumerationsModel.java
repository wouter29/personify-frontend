package be.personify.iam.frontend.wicket.model.common;

import java.util.List;

import org.apache.wicket.model.util.ListModel;

public class EnumerationsModel  extends ListModel<Enum> {

	private static final long serialVersionUID = 4862995885447822353L;

	private List<Enum> enums;

	public EnumerationsModel(List<Enum> enums) {
		super();
		this.enums = enums;
		
	}
	
	@Override
	public List<Enum> getObject() {
	   return enums;
	}
	
	
	public int getCount() {
		return getObject().size();
	}
	
	

	

}
