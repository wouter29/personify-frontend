package be.personify.iam.frontend.wicket.util.wicket;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigationIncrementLink;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigationLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.data.DataView;

public class CustomPagingNavigator extends Panel {

	private static final String PAGING_LINKS_CONTAINER = "pagingLinksContainer";
	private static final String PAGING_NAVIGATOR_NEXT = "PagingNavigator.next";
	private static final String PAGING_NAVIGATOR_PREVIOUS = "PagingNavigator.previous";
	private static final String NEXT = "next";
	private static final String PREV = "prev";

	private static final long serialVersionUID = 2051135263231834431L;
	
	public static final String NAVIGATION_ID = "navigation";
	
	private static final String ITEMS_VALUE = "itemsValue";
	private static final String ITEM_PER_PAGE_LINK = "itemPerPageLink";
	private static final String ITEMS_PER_PAGE = "itemsPerPage";
	private static final String ITEMS_PER_PAGE_CONTAINER = "itemsPerPageContainer";

	private PagingNavigation pagingNavigation;
	private final DataView<?> dataView;
	private final IPagingLabelProvider labelProvider;
	private final List<Integer> itemsPerPageValues;
	private WebMarkupContainer pagingLinksContainer;
	

	/**
	 * Constructor
	 * @param id the id of the component
	 * @param dataView the dataview
	 */
	public CustomPagingNavigator(final String id, final DataView<?> dataView) {
		this(id, dataView, null);
		Injector.get().inject(this);
	}


	
	/**
	 * Constructor
	 * @param id the id
	 * @param dataView the dataview
	 * @param labelProvider the labelprovider
	 */
	public CustomPagingNavigator(final String id, final DataView<?> dataView, final IPagingLabelProvider labelProvider) {
		super(id);
		this.dataView = dataView;
		this.labelProvider = labelProvider;
		this.itemsPerPageValues = calculateItemsPerPageValues(dataView);

		addContainerWithPagingLinks();
		addLinksChangingItemsPerPageNumber();

		Injector.get().inject(this);
	}

	
	@Override
	public boolean isVisible() {
		return dataView.getItemCount() > 0;
	}
	
	
	public final PagingNavigation getPagingNavigation() {
		return pagingNavigation;
	}

	
	private void addContainerWithPagingLinks() {

		pagingLinksContainer = new WebMarkupContainer(PAGING_LINKS_CONTAINER) {
			private static final long serialVersionUID = 5175220996867304910L;

			@Override
			public boolean isVisible() {
				return dataView.getPageCount() > 1;
			}
		};

		pagingNavigation = newNavigation(dataView, labelProvider);
		pagingLinksContainer.add(pagingNavigation);

		// Add additional page links
		// pagingLinksContainer.add(newPagingNavigationLink("first", dataView,0).add(new TitleAppender("PagingNavigator.first")));
		pagingLinksContainer.add(newPagingNavigationIncrementLink(PREV, dataView, -1).add(new TitleAppender(PAGING_NAVIGATOR_PREVIOUS)));
		pagingLinksContainer.add(newPagingNavigationIncrementLink(NEXT, dataView, 1).add(new TitleAppender(PAGING_NAVIGATOR_NEXT)));
		// pagingLinksContainer.add(newPagingNavigationLink("last", dataView,-1).add(new TitleAppender("PagingNavigator.last")));

		add(pagingLinksContainer);
	}

	protected PagingNavigation newNavigation(final IPageable pageable, final IPagingLabelProvider labelProvider) {
		return new PagingNavigation(NAVIGATION_ID, pageable, labelProvider);
	}

	protected AbstractLink newPagingNavigationIncrementLink(String id, IPageable pageable, int increment) {
		return new PagingNavigationIncrementLink<Void>(id, pageable, increment);
	}

	protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, int pageNumber) {
		return new PagingNavigationLink<Void>(id, pageable, pageNumber);
	}
	

	private void addLinksChangingItemsPerPageNumber() {
		WebMarkupContainer container = new WebMarkupContainer(ITEMS_PER_PAGE_CONTAINER);
		ListView<Integer> itemsPerPageList = new ListView<Integer>(ITEMS_PER_PAGE, itemsPerPageValues) {
			
			private static final long serialVersionUID = -184230300072618155L;
			
			@Override
			protected void populateItem(ListItem<Integer> item) {
				Link<Void> itemPerPageLink = new ItemPerPageLink<Void>(ITEM_PER_PAGE_LINK, dataView, pagingLinksContainer, item.getModelObject());
				itemPerPageLink.add(new Label(ITEMS_VALUE, item.getModel()));
				item.add(itemPerPageLink);
			}
		};
		if (dataView.getPageCount() <= 1) {
			container.setVisible(false);
		}
		container.add(itemsPerPageList);
		add(container);
	}
	
	
	
	
	private List<Integer> calculateItemsPerPageValues( final DataView<?> dataView ){
		List<Integer> ll = new ArrayList<Integer>();
		if ( dataView.getItemCount() > 10 ) {
			ll.add(5);
			ll.add(10);
			ll.add(50);
			if ( dataView.getItemCount() > 50 ) {
				ll.add(100);
				if (dataView.getItemCount() > 100 ) {
					ll.add(500);
					if ( dataView.getItemCount() > 500 ) {
						ll.add(1000);
					}
				}
			}
		}
		else {
			ll.add(5);
			ll.add(10);
		}
		return ll;
	}

	
	

	
	private final class TitleAppender extends Behavior {
		
		private static final String TITLE = "title";

		private static final long serialVersionUID = -1447455549955564075L;

		private final String resourceKey;

		public TitleAppender(String resourceKey) {
			this.resourceKey = resourceKey;
		}

		@Override
		public void onComponentTag(Component component, ComponentTag tag) {
			tag.put(TITLE, CustomPagingNavigator.this.getString(resourceKey));
		}
	}
}