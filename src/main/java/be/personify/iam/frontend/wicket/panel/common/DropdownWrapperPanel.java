package be.personify.iam.frontend.wicket.panel.common;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *  A panel containing image
 * @author vanderw
 *
 */
public class DropdownWrapperPanel extends Panel {

	
	private static final long serialVersionUID = 1862783768598264625L;

	/**
	 * Constructor
	 * @param id the id
	 */
	public DropdownWrapperPanel(String id) {
		super(id);
	}
	
	
	/**
	 * a dropdown panel
	 * @param id the id
	 * @param choice the choice
	 */
	public DropdownWrapperPanel(String id, DropDownChoice choice) {
		super(id);
        add(choice);
    }
	
	
}
