package be.personify.iam.frontend.wicket.util.token;

import java.util.Map;

public interface Generator {
	
	public String generate(Map<String, String> configuration) throws Exception;

}
