package be.personify.iam.frontend.wicket.model.common;

import org.apache.wicket.Application;
import org.apache.wicket.Session;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.core.util.lang.PropertyResolverConverter;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.string.Strings;

/**
 * Model for a byte array
 * 
 * @author wouter
 *
 */
public class BytesPropertyModel extends PropertyModel<String> {
	
	
	private static final long serialVersionUID = -7671840561692042866L;


	public BytesPropertyModel( Object modelObject, String expression ) {
		super(modelObject, expression);
	}
	
	
	@Override
	public String getObject() {
		final Object target = getInnermostModelOrObject();
		if (target != null) {
			byte[] b = (byte[])PropertyResolver.getValue(getPropertyExpression(), target);
			if ( b != null ) {
				return new String(b);
			}
		}
		return null;
	}

	@Override
	public void setObject(String object) {
		try {
			byte[] objectToSet = object.getBytes();
			final String expression = propertyExpression();
			if (Strings.isEmpty(expression)) {
				Object target = getTarget();
				if (target instanceof IModel) {
					((IModel)target).setObject(objectToSet);
				}
				else {
					setTarget(objectToSet);
				}
			}
			else {
				PropertyResolverConverter prc = new PropertyResolverConverter( Application.get().getConverterLocator(), Session.get().getLocale());
				PropertyResolver.setValue(expression, getInnermostModelOrObject(), objectToSet, prc);
			}
		}
		catch ( RuntimeException e ) {
			e.printStackTrace();
		}
	}

	
	@Override
	public Class getObjectClass(){
		return String.class;
	}
	

}
