package be.personify.iam.frontend.wicket.panel.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.datetime.PatternDateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.PatternValidator;
import org.springframework.hateoas.EntityModel;

import be.personify.iam.frontend.fontawesome.Icon;
import be.personify.iam.frontend.wicket.model.common.BytesPropertyModel;
import be.personify.iam.frontend.wicket.model.common.WicketEntitiesModel;
import be.personify.iam.frontend.wicket.util.PasswordPolicyValidator;
import be.personify.iam.frontend.wicket.util.wicket.ChoiceRendererUtil;
import be.personify.util.BooleanUtil;
import be.personify.util.StringUtils;
import be.personify.util.ui.CustomRenderer;

/**
 * Generic form panel
 * @author vanderw
 *
 * @param <T> the generic type
 */
public class FormPanel<T> extends Panel {

	private static final String VALUES = "values";
	private static final String _10 = "10";
	private static final String BYTE = "byte[]";
	private static final String TYPE = "type";
	private static final String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";

	private static final String COMPONENT_ID_ROWS = "rows";
	
	private static final long serialVersionUID = -2017066326676226313L;
	
	/**
	 * Constructor
	 * @param id the id of the panel
	 */
	public FormPanel(String id) {
		super(id);
	}
	
	/**
	 * Add a textfield row
	 * @param form The related form
	 * @param resource the resource
	 * @param fieldName the fieldname
	 * @param required is it required?
	 */
	public void addTextFieldRow(Form<T> form, T resource,  String fieldName, boolean required ) {
		addTextFieldRow(form, resource,  fieldName,  required, StringUtils.EMPTY_STRING );
	}
	
	
	public void addCheckboxRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix ) {
		MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		CheckBox tf = new CheckBox(fieldName, new PropertyModel<Boolean>(resource,prefix +fieldName));
		tf.setRequired(required);
		textFieldRow.add(tf);
		form.add( textFieldRow );
	}
	
	
	public void addTextFieldRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix ) {
		MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		TextField<String> tf = new TextField<String>(fieldName, new PropertyModel<String>(resource,prefix +fieldName));
		tf.setRequired(required);
		textFieldRow.add(tf);
		form.add( textFieldRow );
	}
	
	
	public void addTextFieldRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix, String validationPattern ) {
		MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		TextField<String> tf = new TextField<String>(fieldName, new PropertyModel<String>(resource,prefix +fieldName));
		tf.add(new PatternValidator(validationPattern));
		tf.setRequired(required);
		textFieldRow.add(tf);
		form.add( textFieldRow );
	}
	
	
	public void addFileUploadRow(Form<T> form, String fieldName, boolean required, String prefix ) {
		MarkupContainer fileUploadRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		FileUploadField tf = new FileUploadField(fieldName);
		tf.setRequired(required);
		fileUploadRow.add(tf);
		form.add( fileUploadRow );
	}
	
	
	public void addTextAreaRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix, int numberOfRows, Class<?> type ) {
		MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		TextArea<String> textArea = null;
		if ( type == byte[].class) {
			textArea = new TextArea<String>(fieldName, new BytesPropertyModel(resource,prefix +fieldName));
		}
		else {
			textArea = new TextArea<String>(fieldName, new PropertyModel<String>(resource,prefix +fieldName));
		}
		textArea.setRequired(required);
		textArea.add(new AttributeModifier(COMPONENT_ID_ROWS, new Model<String>(StringUtils.EMPTY_STRING + numberOfRows)));
		textFieldRow.add(textArea);
		form.add( textFieldRow );
	}
	
	public void addStringListRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix ) {
		MarkupContainer stringListRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		ListCreatePanel<List<String>> listCreatePanel  = new ListCreatePanel<List<String>>(fieldName, new PropertyModel<List<String>>(resource,prefix +fieldName));
		stringListRow.add(listCreatePanel);
		form.add( stringListRow );
	}
	
	
	
	public void addCustomRendererFieldRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix, String customRenderer ) {
				
		if ( customRenderer.startsWith(CustomRenderer.text_area.name())) {
			String defaultRows = _10;
			Class<?> typeClass = String.class;
			if ( customRenderer.contains(StringUtils.PIPE)) {
				String[] attrs = customRenderer.split(StringUtils.ESCAPED_PIPE);
				for ( int i = 1; i < attrs.length; i++ ) {
					String[] a = attrs[i].split(StringUtils.COLON);
					if ( a[0].equalsIgnoreCase(COMPONENT_ID_ROWS)) {
						defaultRows = a[1];
					}
					else if ( a[0].equalsIgnoreCase(TYPE)) {
						if ( a[1].equals(BYTE)) {
							typeClass = byte[].class;
						}
					}
				}
			}
			addTextAreaRow(form, resource, fieldName, required, prefix, Integer.parseInt(defaultRows), typeClass);
		}
		else if ( customRenderer.startsWith(CustomRenderer.integer_dropdown.name())) {
			List<Integer> choices  = new ArrayList<Integer>();
			if ( customRenderer.contains(StringUtils.PIPE)) {
				String[] attrs = customRenderer.split(StringUtils.ESCAPED_PIPE);
				String[] a = null;
				for ( int i = 1; i < attrs.length; i++ ) {
					a = attrs[i].split(StringUtils.COLON);
					if ( a[0].equalsIgnoreCase(VALUES)) {
						if ( a[1].contains(StringUtils.COMMA)) {
							String[] ss = a[1].split(StringUtils.COMMA);
							for ( int j = 0; j < ss.length; j++) {
								choices.add(Integer.parseInt(ss[j]));
							}
						}
						else if ( a[1].contains(StringUtils.DASH)) {
							String[] ss = a[1].split(StringUtils.DASH);
							if ( ss.length == 2) {
								for ( int j = Integer.parseInt(ss[0]); j <= Integer.parseInt(ss[1]); j++ ) {
									choices.add(j);
								}
							}
							else {
								throw new RuntimeException("incorrect customrenderer arguments, has to be two " + customRenderer);
							}
						}
					}
				}
			}
			MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
			textFieldRow.add(new DropDownChoice<Integer>(fieldName, new PropertyModel<Integer>(resource, prefix + fieldName) ,choices));
			form.add( textFieldRow );
		}
		else if ( customRenderer.startsWith(CustomRenderer.icon.name())) {
			List<String> choices  = new ArrayList<String>();
			String name = null;
			for ( Icon icon : be.personify.iam.frontend.fontawesome.Icon.values()) {
				 name = icon.name();
				 if ( name.contains(StringUtils.UNDERSCORE)) {
					 name = name.replaceAll(StringUtils.UNDERSCORE, StringUtils.DASH);
				 }
				 choices.add(name);
			}
			MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
			textFieldRow.add(new DropDownChoice<String>(fieldName, new PropertyModel<String>(resource, prefix + fieldName) ,choices));
			form.add( textFieldRow );
		}
		else if ( customRenderer.startsWith(CustomRenderer.password.name())) {
			PasswordTextField passwordTextField = new PasswordTextField(fieldName, new PropertyModel<String>(resource,prefix +fieldName));
			if ( customRenderer.contains(StringUtils.PIPE)) {
				String[] attrs = customRenderer.split(StringUtils.ESCAPED_PIPE);
				if ( attrs.length == 2 ) {
					passwordTextField.add(new PasswordPolicyValidator(attrs[1]));
				}
				else {
					throw new RuntimeException("only two values separated by | are allowed. eg password|policy-pattern");
				}
			}
			else {
				passwordTextField.add(new PasswordPolicyValidator());
			}
			passwordTextField.setRequired(required);
			MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
			textFieldRow.add(passwordTextField);
			form.add( textFieldRow );
		}
		else if ( customRenderer.startsWith(CustomRenderer.file_size.name())) {
			TextField<String> textField = new TextField<String>(fieldName);
			textField.setVisible(false);
			MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
			textFieldRow.add(textField);
			form.add( textFieldRow );
		}
		else {
			throw new RuntimeException("custom renderer of type " + customRenderer + " is not supported");
		}
		
		
		
	}
	
	
	
	/**
	 * Adds a boolean dropdown
	 * @param form the related form
	 * @param resource the resource
	 * @param fieldName the fieldname
	 */
	public void addBooleanDropDownRow(Form<T> form, T resource,  String fieldName) {
		addBooleanDropDownRow(form, resource, fieldName, StringUtils.EMPTY_STRING);
	} 
	
	
	
	public void addBooleanDropDownRow(Form<T> form, T resource,  String fieldName, String prefix) {
		MarkupContainer booleanDropDownRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		booleanDropDownRow.add(new DropDownChoice<Boolean>(fieldName, new PropertyModel<Boolean>(resource, prefix + fieldName),BooleanUtil.getBooleansAsList()));
		form.add( booleanDropDownRow );
	} 
	
	
	 
	/**
	 * Adds a datetextfield row
	 * @param form the related form
	 * @param resource the reosurce
	 * @param fieldName the fieldname
	 * @param required is it required?
	 */
	public void addDateTextFieldRow(Form<T> form, T resource,  String fieldName, boolean required) {
		addDateTextFieldRow(form, resource, fieldName, required, StringUtils.EMPTY_STRING);
	}
	
	
	public void addDateTextFieldRow(Form<T> form, T resource,  String fieldName, boolean required, String prefix ) {
		MarkupContainer lastChangedRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		DateTextField dateTextField= new DateTextField(fieldName,new PropertyModel<Date>(resource, prefix + fieldName), new PatternDateConverter(DEFAULT_DATE_PATTERN,true));
		dateTextField.setRequired(required);
		lastChangedRow.add(dateTextField);
		form.add( lastChangedRow );
	}
	
	
	/**
	 * Add a dropdown row
	 * @param form the related form
	 * @param resource the resource
	 * @param fieldName the fieldname
	 * @param choice the choice
	 */
	public void addDropdownRow(Form<T> form, T resource, String fieldName, DropDownChoice<?> choice) {
		MarkupContainer textFieldRow = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		textFieldRow.add(choice);
		form.add( textFieldRow ); 
	}
	
	
	/**
	 * Add a collection dropdown
	 * @param form the related form
	 * @param fieldName the name of the field
	 * @param chainingModel the chaining model
	 * @param resourcesModel the resources model
	 * @param visible is it visible?d
	 * @return a dropdownchoice
	 */
	public DropDownChoice<EntityModel<?>> addCollectionDropdownRow(Form<T> form, String fieldName, IModel chainingModel, WicketEntitiesModel resourcesModel, boolean visible) {
		return addCollectionDropdownRow(form,fieldName, chainingModel, resourcesModel, visible, null);
	}
	
	
	public DropDownChoice<EntityModel<?>> addCollectionDropdownRow(Form<T> form, String fieldName, IModel chainingModel, WicketEntitiesModel resourcesModel, boolean visible, String displayFields) {
		MarkupContainer row = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		row.setOutputMarkupId(true);
		row.setOutputMarkupPlaceholderTag(true);
		DropDownChoice<EntityModel<?>> dropdown = new DropDownChoice<EntityModel<?>>(fieldName, IModel.of(chainingModel), resourcesModel, ChoiceRendererUtil.getDefaultchoiceRenderer(displayFields) );
		dropdown.setOutputMarkupId(true);
		dropdown.setOutputMarkupPlaceholderTag(true);
		row.add(dropdown);
		row.setVisible(visible);
		form.add(row);
		return dropdown;
		
	}
	
	
	
	
	public DropDownChoice<EntityModel<?>> addCollectionDropdownRow(Form<T> form, String fieldName, WicketEntitiesModel resourcesModel, boolean visible) {
		return addCollectionDropdownRow(form, fieldName, resourcesModel, visible, null);
		
	}
	
	
	public DropDownChoice<EntityModel<?>> addCollectionDropdownRow(Form<T> form, String fieldName, WicketEntitiesModel resourcesModel, boolean visible, String displayFields) {
		MarkupContainer row = new WebMarkupContainer(fieldName + PanelConstants.ROW);
		row.setOutputMarkupId(true);
		row.setOutputMarkupPlaceholderTag(true);
		DropDownChoice<EntityModel<?>> dropdown = new DropDownChoice<EntityModel<?>>(fieldName,	resourcesModel,	ChoiceRendererUtil.getDefaultchoiceRenderer(displayFields));
		dropdown.setOutputMarkupId(true);
		dropdown.setOutputMarkupPlaceholderTag(true);
		row.add(dropdown);
		row.setVisible(visible);
		form.add(row);
		return dropdown;
		
	}
	
	
	
	
}
