package be.personify.iam.frontend.wicket.model.common;

import java.util.List;

import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.LoadableDetachableModel;
import org.springframework.hateoas.Link;

public abstract class WicketEntityModel<T> extends LoadableDetachableModel<T> {
	

	private static final long serialVersionUID = -3640634378534872269L;
	
	public String href;
	
	public WicketEntityModel() {
		super();
		Injector.get().inject(this);
	}

	
	public abstract List<Link> getLinks();


	public String getHref() {
		return href;
	}


	public void setHref(String href) {
		this.href = href;
	}


	@Override
	public String toString() {
		return "WicketEntityModel [href=" + href + "]";
	}
	
	
	
	

}
