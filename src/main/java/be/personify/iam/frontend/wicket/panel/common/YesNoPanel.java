package be.personify.iam.frontend.wicket.panel.common;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalDialog;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.StringResourceModel;

/**
 * Basic panel asking for confirmation
 * 
 * @author vanderw
 *
 */
public class YesNoPanel extends Panel {
	
	private static final String FORM = "form";

	private static final long serialVersionUID = 314117754880014563L;
	
	private static final String KEY_MESSAGE = "yesNoMessage";
	private static final String KEY_NOBUTTON = "noButton";
	private static final String KEY_YESBUTTON = "yesButton";
	
	private String messageKey = null;
	private ModalDialog modalWindow = null;

	public YesNoPanel(String id, String messageKey, ModalDialog modalWindow ) {
		super(id);
		this.messageKey = messageKey;
		this.modalWindow = modalWindow;
	}

	
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		//add the message
		add(new Label(KEY_MESSAGE, new StringResourceModel(messageKey)));
		
		Form form = new Form(FORM);
		
		//add the yes button
		Link<String> yesLink = new Link<String>(KEY_YESBUTTON) {
			private static final long serialVersionUID = 5573479047350642963L;
			@Override
			public void onClick() {
					performYesAction();		
			}
		};
		form.add(yesLink);
		
		//add the no button
		AjaxLink<String> noLink = new AjaxLink<String>(KEY_NOBUTTON) {
			private static final long serialVersionUID = -6224695233848043709L;
			@Override
			public void onClick(AjaxRequestTarget target) {
				performNoAction(target);		
			}
		};
		form.add(noLink);
		
		//add the form
		add(form);
		
	}
	
	
	
	public void performYesAction() {
		//override
	}
	
	public void performNoAction( AjaxRequestTarget target ) {
		modalWindow.close(target);
	}
	
	
	
	

}
