package be.personify.iam.frontend.wicket.pages.common;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxChannel;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.extensions.ajax.markup.html.AjaxLazyLoadPanel;

import be.personify.util.StringUtils;

public class ScreenUtils {
	 
	public static final String COMPONENT_ID_CONTENT = "content";
	
	public static AjaxLazyLoadPanel getLazyLoadPanel( String id, Component zz ) {
		AjaxLazyLoadPanel lazyLoadPanel = new AjaxLazyLoadPanel(id) {
            
         	private static final long serialVersionUID = -3987845073768036167L;

         	@Override
         	public Component getLazyLoadComponent(String id) {
         		return zz;
         	}
         	
//         	@Override
//         	public void updateAjaxAttributes(AjaxRequestAttributes attributes){
//         		attributes.setChannel(new AjaxChannel(StringUtils.EMPTY_STRING + System.currentTimeMillis()));
//         	}
         	

		};
		lazyLoadPanel.setOutputMarkupId(true);
		return lazyLoadPanel;
	}

}
