package be.personify.iam.frontend.wicket.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import be.personify.util.StringUtils;

/**
 * Utility class
 * @author vanderw
 *
 */
public class DateUtil {
	
	private static final String DATE_FORMAT = "HH:mm:ss dd/MM/yyyy";
	
	
	/**
	 * Formats a date with default format
	 * @param date the date
	 * @return a string with the formatted date
	 */
	public static String format( Date date ) {
		if ( date != null) {
			return new SimpleDateFormat(DATE_FORMAT).format(date);
		}
		return StringUtils.EMPTY_STRING;
	}
	
	
	
	/**
	 * Formats a date with specified format
	 * @param date the date 
	 * @param dateFormat the date format
	 * @return a string containing the formatted date
	 */
	public static String format( Date date, DateFormat dateFormat ) {
		if ( date != null) {
			return dateFormat.format(date);
		}
		return StringUtils.EMPTY_STRING;
	}
	
	
	
	/**
	 * Formats with a string defining the format ( performance penalty )
	 * @param date the date
	 * @param dateFormatString the format
	 * @return a string with the formated date
	 */
	public static String format( Date date, String dateFormatString ) {
		if ( date != null) {
			return new SimpleDateFormat(dateFormatString).format(date);
		}
		return StringUtils.EMPTY_STRING;
	}
	
	
	public static String format( Date date, String dateFormatString, Locale locale ) {
		if ( date != null) {
			return new SimpleDateFormat(dateFormatString,locale).format(date);
		}
		return StringUtils.EMPTY_STRING;
	}
	
	

}
