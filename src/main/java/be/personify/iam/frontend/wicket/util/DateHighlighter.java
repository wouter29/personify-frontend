package be.personify.iam.frontend.wicket.util;

import java.util.Date;

import be.personify.util.StringUtils;

public class DateHighlighter {
	
	
	private static final String STRING_ESCAPED_DOT = "\\.";
	private static final String STRING_ESCAPED_QMARK = "\\?";
	private static final String NOW = "now";
	private static final String AFTER = "after";

	public static String getClassForPattern( Date date, String pattern ) {
		
		Date now = new Date();
		
		String[] s1 = pattern.split(STRING_ESCAPED_QMARK);
		if ( s1.length != 2) {
			throw new RuntimeException("multiple questions marks detected in pattern " + pattern);
		}
		
		String[] s2 = s1[0].split(STRING_ESCAPED_DOT);
		if ( s2.length != 2 ) {
			throw new RuntimeException("multiple dots detected in first pattern part" + pattern);
		}
		
		String[] s3 = s1[1].split(StringUtils.COLON);
		
		
		if ( s2[0].equals(AFTER)) {
			if ( s2[1].equals(NOW)) {
				if ( date == null ) {
					return s3[0];
				}
				else {
					if ( date.after(now)) {
						return s3[0];
					}
					else {
						return s3[1];
					}
				}
			}
		}
		
		
		
		return StringUtils.EMPTY_STRING;
	}

}
