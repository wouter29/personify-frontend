package be.personify.iam.frontend.wicket.util;

import java.util.Date;
import java.util.Locale;

import org.apache.wicket.markup.html.basic.Label;

/**
 * Basic class for displaying a date
 * @author vanderw
 *
 */
public class DateLabel extends Label {

	private static final long serialVersionUID = 7191335790488417183L;
	
	

	public DateLabel(String id, Date label) {
		super(id, DateUtil.format(label));
	}
	
	public DateLabel(String id, Date label, String dateFormatString) {
		super(id, DateUtil.format(label, dateFormatString));
	}
	
	public DateLabel(String id, Date label, String dateFormatString, Locale locale) {
		super(id, DateUtil.format(label, dateFormatString, locale));
	}

}
