package be.personify.iam.frontend.wicket.panel.common;

import org.apache.wicket.markup.html.image.ContextImage;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *  A panel containing image
 * @author vanderw
 *
 */
public class ImagePanel extends Panel {

	private static final String IMGID = "imgid";
	
	private static final long serialVersionUID = 109788532325281959L;

	/**
	 * Constructor
	 * @param id the id
	 */
	public ImagePanel(String id) {
		super(id);
	}
	
	/**
	 * Constructor with image
	 * @param id the id
	 * @param imgRef the image ref
	 */
	public ImagePanel(String id, String imgRef) {
		super(id);
        add(new ContextImage(IMGID, imgRef));
    }
	
	
}
