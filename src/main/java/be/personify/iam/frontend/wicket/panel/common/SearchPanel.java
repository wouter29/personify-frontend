package be.personify.iam.frontend.wicket.panel.common;



import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import be.personify.iam.api.util.Query;
import be.personify.util.StringUtils;


public abstract class SearchPanel<T> extends FormPanel<T> {

	private static final long serialVersionUID = -1264501761977207670L;
	
	private static final Logger logger = LogManager.getLogger(SearchPanel.class);
	
	private Query query = new Query();
	
	
	private Form<T> form;
	
	public SearchPanel(String id) {
		super(id);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		form = initForm();
		add(form);
	}
	 
	public Query getQuery() {
		return query;
	}
	
	public abstract Form<T> initForm();
	
	public abstract void search(AjaxRequestTarget target);
	
	public abstract File download() throws Exception;
	
	public abstract void composeSearchCriteria();

	
	/**
	 * Adds a add button to the form
	 * @param form the form
	 * @param clazz the class
	 * @return the button to be returned
	 */
	public Button getAddButton(Form<?> form, Class clazz) {
		return new Button(PanelConstants.BUTTON_ADD) {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void onSubmit() {
				setResponsePage(clazz, null);
				super.onSubmit();
			}
			
		};
	}
	
	
	/**
	 * Adds a submit button to the form
	 * @return the ajaxbutton to be returned
	 */
	public AjaxButton getSubmitButton() {
		return new AjaxButton(PanelConstants.BUTTON_SUBMIT) {
			private static final long serialVersionUID = 8752744060226069594L;
			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				composeSearchCriteria();
				search(target);
			}
			
			@Override
		    protected void onError(AjaxRequestTarget target) {
		      logger.error("error occurred {}" ,target);
		    }
		};
	}
	

	public String checkLike(String tokenLike) {
		if ( !tokenLike.contains(StringUtils.PERCENT)) {
			tokenLike = StringUtils.PERCENT + tokenLike + StringUtils.PERCENT;
		}
		return tokenLike;
	}
	
	
	public IModel<File> getFileDownloadModel() {
		IModel<File> fileModel = new Model<File>(){
		    
			private static final long serialVersionUID = 1L;

			public File getObject() { 
		        try {
		        	File f = download();
		        	return f;
		        }
		        catch( Exception e ) {
		        	return null;
		        }
		    }
		};
		return fileModel;
	}
}
