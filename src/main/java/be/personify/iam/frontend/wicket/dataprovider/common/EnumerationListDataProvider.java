package be.personify.iam.frontend.wicket.dataprovider.common;

import java.util.Iterator;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

import be.personify.iam.frontend.wicket.model.common.EnumerationsModel;

public class EnumerationListDataProvider<T> implements IDataProvider<Enum> {
	
	private static final long serialVersionUID = 8041490541757613479L;

	private EnumerationsModel enumerationsModel = null;

	public EnumerationListDataProvider ( EnumerationsModel enumerationsModel ) {
		this.enumerationsModel = enumerationsModel;
	}
	
	@Override
	public void detach() {
		enumerationsModel.detach();
	}

	@Override
	public Iterator<Enum> iterator(long first, long count) {
		return enumerationsModel.getObject().subList(Math.toIntExact(first), Math.toIntExact( first+count)).iterator();
	}

	@Override
	public long size() {
		return enumerationsModel.getObject().size();
	}

	@Override
    public IModel<Enum> model(final Enum object) {
    	
        return new LoadableDetachableModel<Enum>() {

            private static final long serialVersionUID = 1L;

            @Override
            protected Enum load() {
                return object;
            }
        };
    }



}
