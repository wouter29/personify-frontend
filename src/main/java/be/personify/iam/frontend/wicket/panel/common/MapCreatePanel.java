package be.personify.iam.frontend.wicket.panel.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import be.personify.util.KeyValue;
import be.personify.util.StringUtils;

/**
 * Panel for a Map
 * @author vanderw
 *
 * @param <T> the generic type
 */
public class MapCreatePanel<T> extends Panel {
	
	private static final String MAP_PANEL_FORM = "mapPanelForm";

	private static final String DATA_VIEW_CONTAINER = "dataViewContainer";

	private static final String NEW_VALUE = "new_value";

	private static final String NEW_KEY = "new_key";

	private static final String DATA_VIEW = "dataView";
	
	private static final long serialVersionUID = 4051148613584622985L;
	 
	
	List<KeyValue> keyValueList = new ArrayList<KeyValue>();
	private MarkupContainer dataViewContainer;
	private DataView<KeyValue> dataView;
	
	
	public MapCreatePanel( String id, final IModel<T> model ) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		Form form = new Form(MAP_PANEL_FORM);
		
		dataViewContainer = new WebMarkupContainer(DATA_VIEW_CONTAINER);
		dataViewContainer.setOutputMarkupId(true);
		
		Map<String,String> map = (Map<String,String>)getDefaultModel().getObject();
		if ( map != null ) {
			for (String key : map.keySet()) {
				keyValueList.add(new KeyValue(key, map.get(key)));
			}
		}
		
		dataView = getDataView(DATA_VIEW,createDataProvider() );
		dataViewContainer.add(dataView);
		
		TextField<String> newKeyTextField = new TextField<String>(NEW_KEY, new Model<String>(StringUtils.EMPTY_STRING));
		TextField<String> newValueTextField = new TextField<String>(NEW_VALUE, new Model<String>(StringUtils.EMPTY_STRING));
		
		dataViewContainer.add(newKeyTextField);		
		dataViewContainer.add(newValueTextField);
		
		dataViewContainer.add(new AjaxButton(PanelConstants.BUTTON_ADD) {
			private static final long serialVersionUID = -5272734134708203645L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				
				if (  newKeyTextField.getModelObject() != null  && newValueTextField.getModelObject() != null ) {
				
					keyValueList.add(new KeyValue(newKeyTextField.getModelObject(),newValueTextField.getModelObject()));
					
					Map<String,String> mm = (Map<String,String>)MapCreatePanel.this.getDefaultModel().getObject();
					
					mm.put(newKeyTextField.getModelObject(), newValueTextField.getModelObject());
						
					//clear
					newKeyTextField.setModelObject(StringUtils.EMPTY_STRING);
					newValueTextField.setModelObject(StringUtils.EMPTY_STRING);
						
					dataView = getDataView(DATA_VIEW,createDataProvider() );
					dataViewContainer.addOrReplace(dataView);
					target.add(dataViewContainer);
				}

				super.onSubmit(target);
			}
			
		});
		form.add(dataViewContainer);
		
		add(form);
	}

	
	

	/**
	 * Creates a dataprovider
	 * @return a list dataprovider
	 */
	private ListDataProvider<KeyValue> createDataProvider() {
				
		ListDataProvider<KeyValue> dp = new ListDataProvider<KeyValue>(keyValueList) {

			private static final long serialVersionUID = -4570054503756352135L;

			@Override
			protected List<KeyValue> getData() {
				return super.getData();
			}

			@Override
			public IModel<KeyValue> model(KeyValue object) {
				if ( object != null ) {
					for ( KeyValue keyValue : keyValueList) {
						if ( keyValue.getKey() != null && keyValue.getKey().equals(object.getKey())) {
							keyValue.setValue(object.getValue());
							return super.model(keyValue);
						}
					}
				}
				return super.model(object);
			}
			
		};
		return dp;
	} 
	
	
	
	
	
	/**
	 * Creates a dataview
	 * @param name the name of the dataview
	 * @param dp a list with the dataproviders
	 * @return a dataview
	 */
	public DataView<KeyValue> getDataView(String name, ListDataProvider<KeyValue> dp) {

	
		DataView<KeyValue> view = new DataView<KeyValue>(name, dp) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<KeyValue> item) {
				item.add(new Label(PanelConstants.KEY, new Model<String>(item.getModelObject().getKey())));		
				item.add(new TextField<String>(PanelConstants.VALUE, new Model<String>(item.getModelObject().getValue()) {

					private static final long serialVersionUID = -1765624247667019938L;

					@Override
					public void setObject(String object) {
						String key = item.getModelObject().getKey();
						for ( KeyValue keyValue : keyValueList) {
							if ( keyValue.getKey() != null && keyValue.getKey().equals(key)) {
								keyValue.setValue(object);
								((Map<String,String>)MapCreatePanel.this.getDefaultModel().getObject()).put(key, object);
							}
						}
						super.setObject(object);
					}
					
				}));
				item.add(new AjaxButton(PanelConstants.BUTTON_DELETE) {
					
					private static final long serialVersionUID = -5272734134708203645L;

					@Override
					protected void onSubmit(AjaxRequestTarget target) {
						
						keyValueList.remove(item.getModelObject());
						((Map<String,String>)MapCreatePanel.this.getDefaultModel().getObject()).remove(item.getModelObject().getKey());
						dataView = getDataView(DATA_VIEW,createDataProvider() );
						dataViewContainer.addOrReplace(dataView);
						target.add(dataViewContainer);
						super.onSubmit(target);
					}
					
				});
			}
		};
		view.setOutputMarkupId(true);
		view.setOutputMarkupPlaceholderTag(true);
		view.setItemsPerPage(1000);
		return view;
	}
	
	
	

}
