package be.personify.iam.frontend.wicket.panel.common;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

public abstract class UpdatePanel<T> extends FormPanel<T> {

	private static final String FORM_FEEDBACK_PANEL = "formFeedbackPanel";

	private static final long serialVersionUID = -5488631638721733259L;

	private Form<T> form;
	
	public FeedbackPanel feedbackPanel;

	public UpdatePanel(String id) {
		super(id);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		// add the feedbackpanel for the form
		feedbackPanel = new FeedbackPanel(FORM_FEEDBACK_PANEL);
		feedbackPanel.setOutputMarkupId(true);
		feedbackPanel.setOutputMarkupPlaceholderTag(true);
		add(feedbackPanel);

		form = initForm();
		add(form);
	}

	public abstract Form<T> initForm();

	public abstract void update();
}
