package be.personify.iam.frontend.wicket.util;

import java.util.regex.Pattern;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import be.personify.util.StringUtils;

/**
 * 
 * @author wouter
 *
 */
public class PasswordPolicyValidator  implements IValidator<String> {

	private static final long serialVersionUID = 1L;
	
//	(?=.*[0-9]) a digit must occur at least once
//	(?=.*[a-z]) a lower case letter must occur at least once
//	(?=.*[A-Z]) an upper case letter must occur at least once
//	(?=.*[@#$%^&+=]) a special character must occur at least oncer
//	(?=\\S+$) no whitespace allowed in the entire string
//	.{8,} at least 8 characters

	private static final String PASSWORD_PATTERN = "\\A(?=\\S*?[0-9])(?=\\S*?[a-z])(?=\\S*?[A-Z])(?=\\S*?[@#$%^&+=])\\S{8,}\\z";
	
	
	private static final String PASSWORD_POLICY_FAILURE = "policyFailure";

	private final Pattern pattern;
	
	public PasswordPolicyValidator(String passwordPattern) {
		if ( !StringUtils.isEmpty(passwordPattern)) {
			pattern = Pattern.compile(passwordPattern);
		}
		else {
			pattern = Pattern.compile(PASSWORD_PATTERN);
		}
	}

	public PasswordPolicyValidator() {
		pattern = Pattern.compile(PASSWORD_PATTERN);
	}

	@Override
	public void validate(IValidatable<String> validatable) {

		final String password = validatable.getValue();

		if (pattern.matcher(password).matches() == false) {
			error(validatable, PASSWORD_POLICY_FAILURE);
		}

	}

	private void error(IValidatable<String> validatable, String errorKey) {
		ValidationError error = new ValidationError();
		error.addKey(getClass().getName() + StringUtils.DOT + errorKey);
		validatable.error(error);
	}

}