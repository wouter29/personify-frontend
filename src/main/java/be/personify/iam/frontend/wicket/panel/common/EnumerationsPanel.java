package be.personify.iam.frontend.wicket.panel.common;

import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;

import be.personify.iam.frontend.wicket.dataprovider.common.EnumerationListDataProvider;
import be.personify.iam.frontend.wicket.model.common.EnumerationsModel;

public class EnumerationsPanel<T> extends Panel {

	private static final String NAME = "name";

	private static final long serialVersionUID = 4393427992271062106L;

	/**
	 * Constructor
	 * @param id the id
	 */
	public EnumerationsPanel(String id) {
		super(id);
	}

	/**
	 * Constructor
	 * @param id the id
	 * @param e the enumeration
	 */
	public EnumerationsPanel(String id, List<Enum> e) {
		super(id);
	}
	
	
	
	/**
	 * Gets the dataview
	 * @param id the id of the component
	 * @param enums the enums
	 * @return a dataview
	 */
    public DataView<Enum> getDataView( String id, List<Enum> enums) {
     	IDataProvider<Enum> dataProvider = new EnumerationListDataProvider(new EnumerationsModel(enums));
    	setVisible(dataProvider.size() > 0);
    	
		DataView<Enum> view = new DataView<Enum>(id, dataProvider) {
			private static final long serialVersionUID = 1L;
			@Override
			protected void populateItem(Item<Enum> item) {
				add(new Label(NAME, item.getModelObject().name()));
			}
		};
		
		return view;
	}
	

}
