package be.personify.iam.frontend.wicket.model.common;

import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.util.ListModel;

public abstract class WicketEntitiesModel<T>  extends ListModel<T> {

	private static final long serialVersionUID = -3640634378534872269L;
	
	public String href = null;
	
	public WicketEntitiesModel() {
		super();
		Injector.get().inject(this);
	}
	
	
	public void setHref( String href ){
		this.href = href;
	}

	public String getHref() {
		return href;
	}
	
	
	public int getCount() {
		return getObject().size();
	}
	
	

	

}
