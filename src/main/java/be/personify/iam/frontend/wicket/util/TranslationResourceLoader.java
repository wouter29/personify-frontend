package be.personify.iam.frontend.wicket.util;

import java.util.Locale;

import org.apache.wicket.Component;
import org.apache.wicket.Session;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.resource.loader.IStringResourceLoader;


/**
 * A custom translation resource loader
 * @author vanderw
 *
 */
public class TranslationResourceLoader implements IStringResourceLoader {
	
	/**
	 * Constructor
	 */
	public TranslationResourceLoader() {
		Injector.get().inject(this);
	}

	public String loadStringResource(Class<?> clazz, String key, Locale locale, String style) {
		return findResource(locale, key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String loadStringResource(Class<?> clazz, String key, Locale locale, String style, String s2) {
		return findResource(locale, key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String loadStringResource(Component component, String key, Locale locale, String s1, String s2) {
		Locale newLocale = Session.get().getLocale();
		if (component != null) {
			newLocale = component.getLocale();
		}
		return findResource(newLocale, key);
	}

	/**
	 * find a resource
	 * @param locale
	 * @param key
	 * @return the string found
	 */
	private String findResource(Locale locale, String key) {
		return key;
	}
	
	
	
}