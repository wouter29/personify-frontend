package be.personify.iam.frontend.wicket.util.wicket;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.data.DataView;

public class ItemPerPageLink<T> extends Link<T> {

	private static final String ITEMS_PER_PAGE_LINK_ACTIVE = "itemsPerPageLinkActive";

	private static final String CLASS = "class";

	private static final String TITLE = "title";

	private static final long serialVersionUID = 3704690029286228416L;
	
	private final int itemsPerPage;
	private final DataView<?> dataView;
	private final WebMarkupContainer pagingLinksContainer;

	public ItemPerPageLink(final String id, final DataView<?> dataView, WebMarkupContainer pagingLinksContainer, int itemsPerPage) {
		super(id);
		this.dataView = dataView;
		this.pagingLinksContainer = pagingLinksContainer;
		this.itemsPerPage = itemsPerPage;
		
	}
	

	@Override
	protected void onInitialize() {
		super.onInitialize();
		boolean enabled = itemsPerPage != dataView.getItemsPerPage();
		setEnabled(enabled);
		if ( !enabled ) {
			add(AttributeModifier.append(CLASS, ITEMS_PER_PAGE_LINK_ACTIVE));
		}
	}



	@Override
	public void onClick() {
		dataView.setItemsPerPage(itemsPerPage);
		pagingLinksContainer.setVisible(dataView.getPageCount() > 1);
	}

	@Override
	protected void onComponentTag(ComponentTag tag) {
		super.onComponentTag(tag);
		tag.put(TITLE, itemsPerPage);
	}

}
