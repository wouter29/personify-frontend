package be.personify.iam.frontend.wicket.dataprovider.common;

import java.util.Iterator; 

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.springframework.hateoas.EntityModel;

import be.personify.iam.frontend.wicket.model.common.WicketEntitiesModel;

public class EntityListDataProvider<T> implements IDataProvider<EntityModel<T>> {
	
	private static final long serialVersionUID = 8041490541757613479L;

	private WicketEntitiesModel<EntityModel<T>> entitiesModel = null;

	public EntityListDataProvider ( WicketEntitiesModel<EntityModel<T>> resourcesModel ) {
		this.entitiesModel = resourcesModel;
	}
	
	@Override
	public void detach() {
		entitiesModel.detach();
	}

	@Override
	public Iterator<EntityModel<T>> iterator(long first, long count) {
		return entitiesModel.getObject().subList(Math.toIntExact(first), Math.toIntExact( first+count)).iterator();
	}

	@Override
	public long size() {
		return entitiesModel.getObject().size();
	}

	@Override
    public IModel<EntityModel<T>> model(final EntityModel<T> object) {
    	
        return new LoadableDetachableModel<EntityModel<T>>() {

            private static final long serialVersionUID = 1L;

            @Override
            protected EntityModel<T> load() {
                return object;
            }
        };
    }



}
