package be.personify.iam.frontend.wicket.panel.common;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.IModel;
import org.springframework.hateoas.EntityModel;

import be.personify.iam.frontend.wicket.model.common.WicketEntitiesModel;
import be.personify.iam.frontend.wicket.util.wicket.CustomPagingNavigator;

public abstract class EntitiesPanel<T> extends Panel {

	private static final long serialVersionUID = 361376636341741911L;
	public static final long defaultItemsPerPage = 10;
	
	protected SearchPanel<T> searchPanel;
	private Component resultComponent;
	
	public static final String COMP_NAME_SEARCHPANEL = "searchPanel";
	public static final String COMP_NAME_DATAVIEW_HEADER = "dataViewHeader";
	public static final String COMP_NAME_DATAVIEW = "dataView";
	public static final String COMP_NAME_DETAIL_LINK = "detail";
	public static final String COMP_NAME_EDIT_LINK = "edit";
	private static final String PANEL_HEADING = "panel-heading";
	public static final String COMP_NAME_DELETE_LINK = "delete";
	private static final String COMP_NAME_NAVIGATOR = "navigator";
	private static final String COMP_NAME_SEARCH_RESULT = "searchResult";
	
	private MarkupContainer searchResult = new WebMarkupContainer(COMP_NAME_SEARCH_RESULT);
	
	private boolean showHeader = true;
	private boolean searchPanelVisible = true;
	private List<String> componentFilters = null;
	
	public static final String SALT_KEY = "h";

	
	public EntitiesPanel(String id) {
		super(id);
	}
	
	public EntitiesPanel(String id, boolean searchPanelVisible, boolean showHeader, String... componentFilters) {
		super(id);
		this.searchPanelVisible = searchPanelVisible;
		this.componentFilters = Arrays.asList(componentFilters);
		this.showHeader = showHeader;
	}

	public EntitiesPanel(String id, WicketEntitiesModel<EntityModel<T>> model) {
		super(id, model);
	}
	
	public void setCustomModel(IModel model ) {
		setDefaultModel(model);
	}
	
	public void setCustomModel(WicketEntitiesModel<EntityModel<T>> model ) {
		setDefaultModel(model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		searchPanel = composeSearchPanel();
		add(searchPanel);
		
		searchResult = new WebMarkupContainer(COMP_NAME_SEARCH_RESULT);
		searchResult.setOutputMarkupId(true);
		
		MarkupContainer panelHeading = new WebMarkupContainer(PANEL_HEADING);
		panelHeading.setVisible(showHeader);
		searchResult.add(panelHeading);
		
		searchResult.add(getDataViewHeader());
		resultComponent = getDataView();
		searchResult.add(resultComponent);
		searchResult.add(composeNavigator());
		searchResult.setVisible(resultComponent.isVisible());
		add(searchResult);
	}
	
	private Component composeNavigator() {
		Component component = new CustomPagingNavigator(COMP_NAME_NAVIGATOR, (DataView)resultComponent);
		return component;
	}
	
	
	protected void refreshDataView(AjaxRequestTarget target) {
		resultComponent = getDataView();
		searchResult.addOrReplace(resultComponent);
		searchResult.addOrReplace(new CustomPagingNavigator(COMP_NAME_NAVIGATOR, (DataView)resultComponent));
		target.add(searchResult);
	}
	 
	
	public WicketEntitiesModel<EntityModel<T>> getCustomModel() {
		return (WicketEntitiesModel<EntityModel<T>>)getDefaultModel();
	}

	public abstract DataView<EntityModel<T>> getDataView();
	public abstract DataView<String> getDataViewHeader();
	public abstract SearchPanel<T> composeSearchPanel();
	
	public String getPanelName() {
		return this.getClass().getName();
	}
	
	public Component getViewComponent() {
		return resultComponent;
	}
	
	public Component getSearchResult() {
		return searchResult;
	}

	public boolean isSearchPanelVisible() {
		return searchPanelVisible;
	}

	public void setSearchPanelVisible(boolean searchPanelVisible) {
		this.searchPanelVisible = searchPanelVisible;
	}

	public List<String> getComponentFilters() {
		return componentFilters;
	}

	public void setComponentFilters(List<String> componentFilters) {
		this.componentFilters = componentFilters;
	}

	
	
	public boolean haveToShowComponent( String name ) {
    	if ( getComponentFilters() != null && !getComponentFilters().isEmpty() ) {
    		return !getComponentFilters().contains(name);
    	}
    	return true;
    }
	
	

}
