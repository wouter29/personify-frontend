package be.personify.iam.frontend.wicket.panel.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.Link;

import be.personify.iam.frontend.wicket.model.common.WicketEntityModel;
import be.personify.iam.frontend.wicket.model.common.WicketEntitiesModel;
import be.personify.util.StringUtils;

/**
 * Instantiates classes for the frontend
 * 
 * @author vandew
 *
 */
public class ClassInstanciator {
	
	private static final String PANEL = ".panel";

	private static final String MODEL = ".model";

	@SpringBean
	private Environment environment;
	
	private static final Logger logger = LogManager.getLogger(ClassInstanciator.class);
	
	
	/**
	 * Constructor
	 */
	public ClassInstanciator() {
		Injector.get().inject(this);
	}
	
	
	public IModel<?> getModel(String entityName, String href ) {
		Link link = Link.of(href, entityName);
		return getModel(null, link);
	}
	
	
	private IModel<?> getModel(String parentName, Link link) {
		String name = link.getRel() + MODEL;
		if ( !StringUtils.isEmpty(parentName )) {
			name = parentName + StringUtils.DOT + name;
		}
		if ( link.getHref() == null ) {
			throw new RuntimeException ("-------------------name: "+  name  + " href " + link.getHref());
		}
		try {
			
			String className = environment.getProperty(name);
			if ( className == null ) {
				throw new RuntimeException("className is null for name " + name);
			}
			Class<?> clazz = Class.forName(environment.getProperty(name));
			logger.debug("find class for modelName {} {}", name, clazz);
			IModel<?> model = (IModel<?>)clazz.getDeclaredConstructor().newInstance();
			logger.debug("find model {}", model);
			if ( model == null ) {
				throw new RuntimeException("model is null for " + parentName);
			}
			if ( model instanceof WicketEntityModel ){
				((WicketEntityModel<?>)model).setHref(link.getHref());
			}
			else if (model instanceof WicketEntitiesModel ){
				((WicketEntitiesModel<?>)model).setHref(link.getHref());
			}
			
			return model;
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	private Panel getPanel(String parentName, String name, IModel<?> model, boolean withRelations) {
		return getPanel(null, parentName, name, model, withRelations, false);
	}
	
	
	
	private Panel getPanel(String wicketPanelName, String parentName, String name, IModel<?> model, boolean withRelations, boolean expanded) {
		if ( model != null ) {
			String panelName = name + PANEL;
			if ( !StringUtils.isEmpty(parentName )) {
				panelName = parentName + StringUtils.DOT + panelName;
			}
			if (StringUtils.isEmpty(wicketPanelName)) {
				wicketPanelName = name;
			}
			try {
				Class<?> clazz = Class.forName(environment.getProperty(panelName));
				logger.debug("find class for panelName {} {}", panelName, clazz);
				Class<?>[] cArg = new Class[1]; 
				cArg[0] = String.class; 
				Object o = clazz.getDeclaredConstructor(cArg).newInstance(wicketPanelName);
				if ( o instanceof EntityPanel ) {
					EntityPanel<?> r = (EntityPanel<?>)o;
					r.setCustomModel(model); 
					r.setShowRelations(withRelations);
					r.setParentName(parentName);
					r.setExpanded(expanded);
					return r;
				}
				else if ( o instanceof EntitiesPanel ) {
					EntitiesPanel<?> r = (EntitiesPanel<?>)o;
					r.setCustomModel(model); 
					return r;
				}
			}
			catch( Exception e ) {
				e.printStackTrace();
			}
		}
		logger.debug("the given model is null for name {}", name);
		return null;
	}
	
	

	/**
	 * Gets a panel
	 * @param parentName p
	 * @param link link
	 * @param withRelations w
	 * @return a panel
	 */
	public Panel getPanel(String parentName, Link link, boolean withRelations) {
		IModel<?> model = getModel(parentName, link);
		if ( model == null) {
			logger.debug("the model is null for link {}", link);
		}
		else {
			logger.debug(" model {} not null for link {}", model, link);
		}
		return getPanel(parentName, link.getRel().value(), model, withRelations);
	}
	
	
	
	/**
	 * Gets a panel
	 * @param panelName p
	 * @param entityName e
	 * @param href h
	 * @param withRelations w
	 * @param expanded e
	 * @return a panel
	 */
	public Panel getPanel(String panelName, String entityName, String href, boolean withRelations, boolean expanded) {
		IModel<?> model = getModel(entityName, href);
		if ( model == null) {
			logger.debug("the model is null for href {}", href);
		}
		else {
			logger.debug(" model {} not null for link {}", model, href);
		}
		return getPanel(panelName, null, entityName, model, withRelations, expanded);
	}
	

}
