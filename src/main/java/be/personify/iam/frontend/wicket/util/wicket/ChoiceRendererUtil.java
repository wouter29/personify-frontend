package be.personify.iam.frontend.wicket.util.wicket;

import java.util.List;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.model.IModel;
import org.springframework.hateoas.EntityModel;

import be.personify.iam.frontend.wicket.util.DisplayNameDetector;

public class ChoiceRendererUtil {
	
	
	public static ChoiceRenderer<EntityModel<?>> getDefaultchoiceRenderer() {
		return new ChoiceRenderer<EntityModel<?>>() {

			private static final long serialVersionUID = 4304333640340427580L;

			@Override
			public Object getDisplayValue(EntityModel<?> object) {
				return DisplayNameDetector.detectDisplayName(object.getContent());
			}
	
			@Override
			public EntityModel<?> getObject(String id,	IModel<? extends List<? extends EntityModel<?>>> choices) {
				return super.getObject(id, choices);
			}
		};
	}
	
	
	public static ChoiceRenderer<EntityModel<?>> getDefaultchoiceRenderer(String displayFields) {
		return new ChoiceRenderer<EntityModel<?>>() {

			private static final long serialVersionUID = 4304333640340427580L;

			@Override
			public Object getDisplayValue(EntityModel<?> object) {
				return DisplayNameDetector.detectDisplayName(object.getContent(), displayFields);
			}
	
			@Override
			public EntityModel<?> getObject(String id,	IModel<? extends List<? extends EntityModel<?>>> choices) {
				return super.getObject(id, choices);
			}
		};
	}

}
