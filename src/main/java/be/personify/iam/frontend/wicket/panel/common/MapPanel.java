package be.personify.iam.frontend.wicket.panel.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;

import be.personify.util.KeyValue;

public class MapPanel extends Panel {

	private static final String OBFUSCATED_PASS = "******************";

	private static final String PASSWORD = "password";
	
	private static final String SECRET = "secret";

	private static final String VALUE = "value";

	private static final String KEY = "key";

	private static final long serialVersionUID = 3718617781557193878L;

	private static final Logger logger = LogManager.getLogger(MapPanel.class);

	public MapPanel(String id, Map<String, String> map) {
		super(id);
		logger.debug("new MapPanel {}", id);
		add(getDataView("dataView", map));
		Injector.get().inject(this);
	}

	public DataView<KeyValue> getDataView(String name, Map<String, String> map) {

		List<KeyValue> keyValueList = new ArrayList<KeyValue>();
		for (String key : map.keySet()) {
			keyValueList.add(new KeyValue(key, map.get(key)));
		}

		DataView<KeyValue> view = new DataView<KeyValue>(name, new ListDataProvider<KeyValue>(keyValueList)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<KeyValue> item) {
				item.add(new Label(KEY, new Model<String>(item.getModelObject().getKey())));
				if ( item.getModelObject().getKey().toLowerCase().contains(PASSWORD)) {
					item.add(new Label(VALUE, new Model<String>(OBFUSCATED_PASS)));
				}
				else if (item.getModelObject().getKey().toLowerCase().contains(SECRET)) {
					item.add(new Label(VALUE, new Model<String>(OBFUSCATED_PASS)));
				}
				else {
					item.add(new Label(VALUE, new Model<String>(item.getModelObject().getValue())));
				}
			}
		};

		view.setItemsPerPage(10);
		return view;
	}

}
