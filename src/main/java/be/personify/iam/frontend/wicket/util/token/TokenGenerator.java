package be.personify.iam.frontend.wicket.util.token;

import java.security.SecureRandom;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TokenGenerator implements Generator {
	 
	private static final Logger logger = LogManager.getLogger(TokenGenerator.class);
	
	public static final String KEY_LENGTH = "length";
	
	public static final int DEFAULT_LENGTH = 6; 
	
	
	protected static SecureRandom random = new SecureRandom();
    

	@Override
	public synchronized String generate(Map<String, String> configuration) throws Exception {
		int length = DEFAULT_LENGTH;
		if ( configuration.get(KEY_LENGTH) != null) {
			length = Integer.parseInt((String)configuration.get(KEY_LENGTH));
			logger.debug("length given in config {}", KEY_LENGTH );
		}
		
		long longToken = Math.abs( random.nextLong() );
        String random = Long.toString( longToken, length );
			
		return random;
	}

}
