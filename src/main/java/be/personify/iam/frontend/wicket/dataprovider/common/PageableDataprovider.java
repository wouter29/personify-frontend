package be.personify.iam.frontend.wicket.dataprovider.common;

import java.util.Iterator;

import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

import be.personify.util.StringUtils;


public abstract class PageableDataprovider<T> extends SortableDataProvider<T,String> {

	private static final String DESC = "desc";
	private static final String ASC = "asc";

	private static final String ID = "id";

	private static final long serialVersionUID = -1931929838891057151L;
	    
	    public long totalCount = 0;

	    public PageableDataprovider() {
	    	super();
	        setSort(ID, SortOrder.ASCENDING);
	        Injector.get().inject(this);
	    }

	    @Override
	    public abstract Iterator<T> iterator(long first, long count);
 
	    @Override
	    public abstract long size();
	    
	    
	    @Override
	    public IModel<T> model(final T object) {
	    	
	        return new LoadableDetachableModel<T>() {

	            private static final long serialVersionUID = 1L;

	            @Override
	            protected T load() {
	                return object;
	            }
	        };
	    }

		@Override
		public void detach() {
			
		}
	    
		
		
		public String getSortString() {
			if ( getSort().getProperty() != null ) {
				return getSort().getProperty() + StringUtils.COMMA + ( getSort().isAscending() ? ASC : DESC ); 
			}
			return null;
		}
	    
	
}
