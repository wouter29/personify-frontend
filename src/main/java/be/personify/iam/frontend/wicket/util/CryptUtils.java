package be.personify.iam.frontend.wicket.util;

import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

public class CryptUtils {
	
	private static final String password = "gaanMetDieBanaan";
	
	
	public static String encrypt(String plainText, String salt) {
		TextEncryptor encryptor = Encryptors.text(password, salt);
		return encryptor.encrypt(plainText);
	}

	public static String decrypt(String encryptedText, String salt) {
		TextEncryptor encryptor = Encryptors.text(password, salt);
		return encryptor.decrypt(encryptedText);
	}
	
	

}
