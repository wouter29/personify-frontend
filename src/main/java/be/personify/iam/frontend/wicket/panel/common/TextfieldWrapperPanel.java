package be.personify.iam.frontend.wicket.panel.common;



import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *  A panel containing a textfield
 * @author vanderw
 *
 */
public class TextfieldWrapperPanel extends Panel {

	
	private static final long serialVersionUID = 6142522085425832227L;

	/**
	 * Constructor
	 * @param id the id
	 */
	public TextfieldWrapperPanel(String id) {
		super(id);
	}
	
	/**
	 * A textfield wrapper panel
	 * @param id the id
	 * @param textField the textfield
	 */
	public TextfieldWrapperPanel(String id, TextField<String> textField) {
		super(id);
        add(textField);
    }
	
	
}
