package be.personify.iam.frontend.wicket.util;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import be.personify.util.StringUtils;

public class DisplayNameDetector {
	
	
	private static final Logger logger = LogManager.getLogger(DisplayNameDetector.class);

	
	private static String[] stringz = new String[] {"name","lastName|firstName","description","sn|givenName","cn|sn", "path|domain", "code"}; 
	
	/**
	 * Detecting display names
	 * @param o the object
	 * @return a string with the displayname
	 */
	public static String detectDisplayName( Object o ) {
		PropertyUtilsBean pub = new PropertyUtilsBean();
		return tryString(o, pub, stringz);
	}
	
	
	public static String detectDisplayName( Object o, String fields ) {
		if ( fields != null ) {
			PropertyUtilsBean pub = new PropertyUtilsBean();
			return tryString(o, pub, fields);
		}
		return detectDisplayName(o);
	}
	

	private static String tryString(Object o, PropertyUtilsBean pub, String... toTries ) {
		for ( String toTry : toTries) {
			logger.debug("trying string {}", toTry);
			if ( !toTry.contains(StringUtils.PIPE)){
				try {
					Object property = pub.getProperty(o, toTry);
					if ( property != null ) {
						logger.debug("found string {} {}", toTry, property);
						return property.toString();
					}
				}
				catch( Exception e ) {
					//e.printStackTrace();
					//return null;
				}
			}
			else {
				logger.debug("concatenation found");
				String[] parts = toTry.split(StringUtils.ESCAPED_PIPE);
				logger.debug("found parts {}", parts.length);
				String result = "";
				for ( int i = 0 ; i < parts.length; i++ ) {
					try {
						Object property = pub.getProperty(o, parts[i]);
						if ( property != null ) {
							logger.debug("found string {} {}", parts[i], property);
							if ( i != 0 ) {
								result = result + StringUtils.SPACE;
							}
							result = result + property.toString();
						}
					}
					catch( Exception e ) {
						//e.printStackTrace();
						//return null;
					}
				}
				logger.debug("yeah returning {}", result);
				return result;
			}
			
		}
		return null;
	}
	

}
