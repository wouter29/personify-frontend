package be.personify.iam.frontend.wicket.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Assert;
import org.junit.Test;

public class DateHighlighterTest {
	
	

	@Test
	public void testExpiry() {
		
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -1);
	
		String pattern = "after.now?date_in_range:date_expired";
		
		String styleClass = DateHighlighter.getClassForPattern(c.getTime(), pattern);
		
		Assert.assertTrue(styleClass.equals("date_expired"));
		
		
	}
	
	
	
	@Test
	public void testFuture() {
		
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		c.add(Calendar.MONTH, 10);
	
		String pattern = "after.now?date_in_range:date_expired";
		
		String styleClass = DateHighlighter.getClassForPattern(c.getTime(), pattern);
		
		Assert.assertTrue(styleClass.equals("date_in_range"));
		
		
	}

}
